#!/bin/bash

pl_Path=$1
pl_Name=${pl_Path##*/}
pl_Host=""
current_Path=$(pwd)
current_Dir=${current_Path##*/}
file_Path=""
episode=""
episode_number="0"
season_number="0"
echo $current_Path
echo $current_Dir

dl_file () {
	ffmpeg -i "$1" -c copy -bsf:a aac_adtstoasc "$2"
}
scan_host () {
	local var1=${pl_Path#*//}
	pl_Host=${var1%%/*}
}
scan_cwtv () {
	IFS='-' read -a pl_Array <<< "$pl_Name"
	show_Name=${pl_Array[0]}
	echo $pl_Array
	case "$show_Name" in
	TheFlash) show_Name="The-Flash"
		;;
	esac
	season_number=${pl_Array[1]:0:1}
	episode_number=${pl_Array[1]:1:2}
}
manual_fill () {
	show_Name=$(dialog --inputbox "Enter show name, please" 10 60 3>&1 1>&2 2>&3 3>&-)
	season_number=$(dialog --inputbox "Enter season number, please" 10 30 3>&1 1>&2 2>&3 3>&-)
	episode_number=$(dialog --inputbox "Enter episode number, please" 10 30 3>&1 1>&2 2>&3 3>&-)
}
filepath_builder () {
	local var1="${current_Path}/${show_Name}/${show_Name}_"
	local var2=""
	local var3=""
	if [ "$season_number" -lt "10" ] 
	then
		var2=S0${season_number}
	else
		var2=S${season_number}
	fi
	var3=E${episode_number}
	episode=${var2}${var3}	
	file_Path="${var1}${var2}${var3}.mp4"
}
scan_host
case "$pl_Host" in
	hlsioscwtv.warnerbros.com) 
		scan_cwtv		
		;;
	*)
		manual_fill
		;;
esac

filepath_builder

dialog --title "Info" --msgbox "Show Name: ${show_Name}\nEpisode: ${episode}\nFile Path: ${file_Path}" 7 80

if [ "$current_Dir" != "TV" ]
	then
		dialog --title 'Error' --msgbox 'Not in TV Folder' 5 20
	else
	    if [ ! -d "${current_Path}/${show_Name}" ]
		then
			mkdir "${current_Path}/${show_Name}"
		fi
		dl_file $pl_Path $file_Path
fi